var request = require('request');

/**
 * 微信公众平台API
 */

/**
 * 获取 Access Token
 * @param {Function} callback(response)
 * 有效期：7200秒
 * 长度：至少要保留512个字符
 * 请求地址：https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
 * 请求协议：https
 * 请求参数：grant_type 必需 "client_credential"
 * 请求参数：appid 必需
 * 请求参数：secret 必需
 * 成功返回：{"access_token":"ACCESS_TOKEN","expires_in":7200}
 * 成功返回：{"access_token":"YUUkNN4rrUoGl4j-L_d3UdJK-Uiw2x8-XX_0WRbop1AHfCC_Fpwe4o-OamnC3FdWsasnMRNBxNj8Z5jbYV-yHg","expires_in":7200}
 * 错误返回：{"errcode":40013,"errmsg":"invalid appid"}
 * 请求频率：2000次/天
 */
exports.requestAccessToken = function (appid, appSecret, callback) {
  if (arguments.length < 3) {
    throw new Error('[error] missing arguments');
  }

  var queries = [];

  queries.push('grant_type=client_credential');
  queries.push('appid=' + appid);
  queries.push('secret=' + appSecret);

  request({
    url: 'https://api.weixin.qq.com/cgi-bin/token?' + queries.join('&'),
    method: 'GET'
  }, function (err, res, body) {
    var response = JSON.parse(body);

    /**
     * 微信返回错误结果
     */
    if (response.errcode) {
      throw new Error('[error] request access token: ' + response.errmsg);
    }

    callback(response);
  });
}

/**
 * 微信自定义菜单创建接口
 * 请求地址：https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN
 * 请求协议：https
 * 请求方法：POST
 * 请求参数：access_token 必需
 * 自定义菜单类型：click 和 view
 * 一级菜单个数限制：3个，不超过16个字节
 * 一级菜单文字限制：4个汉字
 * 二级菜单个数限制：5个
 * 二级菜单文字限制：7个汉字，不超过40个字节，多出来的以“...”代替
 * 请求频率：1000次/天
 */
exports.createCustomMenu = function (accessToken, menu, callback) {
  if (arguments.length < 3) {
    throw new Error('[error] missing arguments');
  }

  var queries = [];

  queries.push('access_token=' + accessToken);

  request({
    url: 'https://api.weixin.qq.com/cgi-bin/menu/create?' + queries.join('&'),
    body: JSON.stringify(menu),
    method: 'POST'
  }, function (err, res, body) {
    var response = JSON.parse(body);

    /**
     * 返回错误结果
     */
    if (response.errcode) {
      throw new Error('[error] create custom menu: ' + response.errmsg);
    }

    callback(response);
  });
}
