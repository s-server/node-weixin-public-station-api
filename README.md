weixin-public-station-api
==========

Weixin public station api.

## Installation

```bash
npm install weixin-public-station-api
```

---

Copyright (c) 2013 Shallker Wang - MIT License (enclosed)
